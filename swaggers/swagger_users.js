/**
 * @swagger
 * /users:
 *  get:
 *      tags:
 *        - Users
 *      summary: Resturns Users
 *      description: List of users
 *      produces:
 *          application/json
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Fail.
 */