/**
 * @swagger
 * /:
 *  get:
 *      tags:
 *        - Base URL
 *      summary: Base Route
 *      description: Base route of application
 *      produces:
 *          application/json
 *      responses:
 *          200:
 *              description: Application is running.
 *          500:
 *              description: Application is not running.
 */