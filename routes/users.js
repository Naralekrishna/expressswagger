var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  try {
    res.status(200).json({ name: `Jonny D` });
  } catch (error) {
    res.status(500).json({ message: `Server Error` });
  }
});

module.exports = router;
